# Smart City: Waste menagement


## Data model
- mettere il tutto in un git separato
- mettere gli user dal backend
- modificare i citizen con degli id user giusti, al momento sono dei placeholder

## IoT Service
- modificare l'immagine
- sistemare meglio il tutto


# Altro

## Performance rifiuti
- hp: i volumi dei bidoni sono uguali (altrimenti si dovrebbe implementare un altro algoritmo)
- Algoritmo in python che gira a cadenza giornaliera su aws lamda

### Algoritmo
- prende tutte le misure in uno slot temporale ∆t di 6 mesi
- per ogni giorno seleziona il riempimento max ottenendo fillingLevelMax per ogni cittadino
- per ogni cittadino calcola la performance individuale
- modifica il valore nel database mongo

### Pseudocode:
userFilling = []

for ti in ∆t:

$`fillintTotal_{ti} = fillingLevelMax_{ti}^{unsorted}+ 
    fillingLevelMax_{ti}^{card}+\\+fillingLevelMax_{ti}^{plastic}+ 
    fillingLevelMax_{ti}^{glass_metal}+fillingLevelMax_{ti}^{organic}`$

$`userFilling.append(fillintTotal_{ti})`$

performanceIndicator = median(userFilling)

> Output

![calculatePerformance](img_readme/calculatePerformance.png)

![calculatePerformancemMongo](img_readme/calculatePerformancemMongo.png)
