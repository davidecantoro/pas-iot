from flask import Flask
from flask import request as flask_request
import json
import logging
from pymongo import MongoClient
from datetime import datetime
import requests



app = Flask(__name__)

def setup_logging():
    # Create a logger object
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Set the log format
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # Create a log handler to print logs on stdout
    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)

    # Add the log manager to the logger 
    logger.addHandler(stdout_handler)

    return logger


logger = setup_logging()

url = "https://dfaxe8nxs4.execute-api.us-east-1.amazonaws.com/dev/users/authenticate"


# Leggi le credenziali dal file
with open('constant.txt', 'r') as f:
    lines = f.readlines()
    username = lines[0].split(':')[1].strip()
    password = lines[1].split(':')[1].strip()

# Crea il dizionario data
data = {
    "username": username,
    "password": password
}


headers = {
    "Cache-Control": "no-cache",
    "Content-Type": "application/json",
    "User-Agent": "PostmanRuntime/7.36.1",
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate, br",
    "Connection": "keep-alive"
}

response = requests.post(url, data=json.dumps(data), headers=headers)
response_data = response.json()
token_jwt = response_data["jwt"]

@app.route('/measure', methods=['POST'])
def handler_measure():
    """
    Handle measurement data.
    """

    data = flask_request.get_json()
    #logging.info("Received data: %s", json.dumps(data))
    logger.info("Received data: %s", json.dumps(data))

    string_format = "%Y-%m-%d %H:%M:%S"
    data["timestamp"] = datetime.strptime(data["timestamp"], string_format)

    data["timestamp"] = data["timestamp"].isoformat()



    url_send_measure = "https://64xqhz017j.execute-api.us-east-1.amazonaws.com/dev/measurements/"
    headers = {
        "Authorization": f"Bearer {token_jwt}",
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
        "User-Agent": "PostmanRuntime/7.36.1",
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive"
    }
    try:
        response = requests.post(url_send_measure, headers=headers, json=data)
        
        if 200 <= response.status_code < 300:
            return json.dumps({"message": f"Data sent successfully: {response.text}"})
        
        else:
            logger.error(f"Error while sending data: {response.text}")
            return json.dumps({"message": f"Error sending data: {response.text}"})
    
    except Exception as e:
        logger.error("Exception while sending data: %s", str(e))
        return json.dumps({"message": "Error sending data"})
        


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
