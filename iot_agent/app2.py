from flask import Flask, request


app = Flask(__name__)

@app.route('/hello', methods=['POST'])
def hello_world():
    print("Hello World")
    return "Hello World"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
