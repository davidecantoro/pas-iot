from flask import Flask
from flask import request as flask_request
import json
import logging
from pymongo import MongoClient
from datetime import datetime




app = Flask(__name__)

def setup_logging():
    # Create a logger object
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Set the log format
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # Create a log handler to print logs on stdout
    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)

    # Add the log manager to the logger 
    logger.addHandler(stdout_handler)

    return logger


logger = setup_logging()



# MongoDB settings

#mongo_url = 'mongodb://localhost:27017/'
#mongo_url = 'mongodb://my-db:27017/'
mongo_url = 'mongodb://172.18.0.2:27017/'

database = 'mydatabase'
collection_measure = 'ts_measurements'

# Connect to MongoDB
client = MongoClient(mongo_url)
db = client[database]  


@app.route('/measure', methods=['POST'])
def handler_measure():
    """
    Handle measurement data.
    """

    data = flask_request.get_json()
    #logging.info("Received data: %s", json.dumps(data))
    logger.info("Received data: %s", json.dumps(data))

    string_format = "%Y-%m-%d %H:%M:%S"
    data["timestamp"] = datetime.strptime(data["timestamp"], string_format)


    # Save data in the MongoDB database


    collection = db[collection_measure] 
    try:
        collection.insert_one(data)

        # Aggregazione e bucketing basato su idBin
        # Se si vogliono aggregare le misurazioni in bucket
        # aggregation_pipeline = [
        #     {
        #         "$match": {
        #             "idBin": data["idBin"]
        #         }
        #     },
        #     {
        #         "$bucketAuto": {
        #             "groupBy": "$timestamp",
        #             "buckets": 5,  # Numero di bucket
        #             "output": {
        #                 "totalMisurazioni": { "$sum": 1 },
        #                 "valoreMedio": { "$avg": "$fillingLevel" }
        #             }
        #         }
        #     }
        # ]
        
        # aggregation_result = collection.aggregate(aggregation_pipeline)
        # for result in aggregation_result:
        #     logger.info("Aggregation result: %s", result)

        # ---------------
        return json.dumps({"message": "Data saved successfully"})
    except Exception as e:
        logger.error("Error while saving data: %s", str(e))
        return json.dumps({"message": "Error saving data"})
        


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
