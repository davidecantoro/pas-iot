# IoT service
Servizio IoT composto dai microservizi:
- iot agent
- iot device

![sketch_architecture](img_readme/sketch_architecture.png)

Il microservizio iot device si occupa di orchestrare l'acquisizione di dati dai waste container e comunicare al gateway di quartiere tramite il protocollo pub/sub (publisher/subscriber) le misure.
Successivamente il gateway si occuperà di comunicare le misurazioni al iot agent, il quale ha il compito di inviare le osservazioni al microservizio adibito al salvataggio di misure sul database.

iot sensor è un simulatore di sensori iot presenti nei waste bin, si occupa di generare le varie osservazioni e comunicarle tramite il protocollo pub/sub.

Questi microservizi sono stati implementati in python facendo uso della programmazione orientata ad oggetti (OOP).


---

I servizi iot usano la rete esterna “rabbitmq_go_net”. 
Quindi prima di far partire il tutto è necessario crearla.

```bash
sudo docker network create go_net
```

Successivamente si possono far partire i container

- flask (IoT agent)
- gateway
- sensors
- rabbitmq



> Output di iot_device
> 


![iot_device_output](img_readme/iot_device_output.png)

Il sensore genera una misura, il gateway la vede e la manda tramite API al iot_agent.

Sensore e gateway usano rabbitmq, con modalità publisher-subscribe.

> Output di iot_agent
> 

![iot_agent_output](img_readme/iot_agent_output.png)
Quando l'IoT Agent riceve una misura va a mandarla tramite API al servizio esterno Allarmi, il quale si occuperà di salvare la misura.
