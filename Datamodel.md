# Data model

For the data models relating to waste containers (waste container and waste container model), reference was made to the standards provide by FIWARE.

Custom datamodels were used for: measure waste bin, user, path, payment, citizens and employee.

# Waste container

This data model refers to the waste container for menagerial purposes.

https://fiware-datamodels.readthedocs.io/en/test_next_version/WasteManagement/WasteContainer/doc/spec/index.html

```json
{
   "id": "wastecontainer:Lecce:005",
   "type": "Unsorted waste",
   "refWasteContainerModel": "wastecontainermodel:c1",
   "serialNumber": "ab55kjl"
}
```

`id` → id: wastecontainer : [city] : [bin id]
`type` : type of rubbish
- Unsorted waste
- Cardboard waste
- Plastic waste
- Glass and metals waste
- Organic waste

`refWasteContainerModel` :(reference)  reference to container model: wastecontainermodel : [container model].
`serialNumber` : serial number

# Waste container model

The waste container model datamodel refers to the data concerning the technical specifications of a waste container.

https://fiware-datamodels.readthedocs.io/en/test_next_version/WasteManagement/WasteContainerModel/doc/spec/index.html

```json
{
 "id": "wastecontainermodel:c1",
  "width": 0.50,
  "height": 0.80,
  "depth": 0.40,
  "cargoVolume": 150,
  "brandName": "Contenedores Ejemplo",
  "modelName": "C1",
  "compliantWith": ["UNE-EN 840-2:2013"],
  "madeOf": "plastic"
}
```

`id` : Unique identifier.
`width`. Width of the container.
`height`. Height of the container.
`height`. Height of the container.
`cargoVolume`. Total volume the container can hold
`brandName`. Name of the brand.
`modelName`. Name of the model.
`compliantWith`. A list of standards to which the container is compliant with (ex. UNE-EN 840-2:2013)
`madeOf`. Material the container is made of. (`plastic`, `wood` , `metal`, `other` )

# Measure waste bin: measurements

Datamodel concerning the observations on the waste container filling percentage.

```json
 {
  "timestamp": {
    "$date": "2023-07-29T09:46:30.000Z"
  },
  "idBin": "wastecontainer:Lecce:005",
  "fillingLevel": 0.1115,
}
```

`idBin` : (reference) Unique identifier of a bin
`timestamp` : timestamp referring to the last measurement
`fillingLevel` : percentage indicating how full the bin is 

# User

Datamodel characterising the users in the application.

```json
{
  "id": "64d664d65ba90f270c40914a",
  "name": "Mario",
  "surname": "Rossi",
  "email": "mariorossi@gmail.com",
  "birthDate": "2003-07-29",
  "username": "mrossi",
  "password": "12345",
  "role" : "admin"
}
```



# Citizen

```json
{
  "id": "452b78ba8b7a89",
  "citizen": "64d664d65ba90f270c40914a",
  "streetAddress": "Via Arcivescovo Petronelli, 8",
  "locality" : "Lecce",
  "performanceIndicator" : "0.3",
  "bins" : ["wastecontainer:Lecce:000","wastecontainer:Lecce:001","wastecontainer:Lecce:002","wastecontainer:Lecce:003","wastecontainer:Lecce:004"]
}
```
`citizen` : (reference) id of the user
`streetAddress` : address for which the citizen is registered
`locality` : city referring to the address
`bins` : (reference) List of bins owned by the user
`performanceIndicator` : daily calculated value, indicator of citizen's waste separation performance



# Vehicle
https://fiware-datamodels.readthedocs.io/en/stable/Transportation/Vehicle/Vehicle/doc/spec/index.html
```json
{
    "id": "vehicle:WasteManagement:01",
    "type": "Vehicle",
    "vehicleType": "lorry",
    "category": ["municipalServices"],
    "name": "C Recogida 1",
    "cargoWeight": 314,
    "serviceStatus": "onRoute",
    "serviceProvided": ["garbageCollection", "wasteContainerCleaning"],
    "areaServed": "Lecce",
    "refVehicleModel": "vehiclemodel:econic",
    "vehiclePlateIdentifier": "3456ABC"
}
```

`serviceProvided`: types of services made available to the vehicle
- garbageCollection
- wasteContainerCleaning

# Employee (deprecated)

Datamodel characterising the employee in the application.

```json
{
  "id": "ba666904a90f4d270c4d6541",
  "user" : "64d664d65ba90f270c40914a",
  "employeeType" : ["Driver"],
  "workFor" : "Monteco",
}
```
`citizen` : (reference) id of the user
`employeeType` : list of the types of work the worker can do
`workFor` : company the employee works for

# Path
Datamodel containing the path to be followed by a Driver.

```json
{
  "id": "c414d650f26647ba909a64d0",
  "hasRoute": ["wastecontainer:Lecce:000","wastecontainer:Lecce:005"],
  "hasDay" : "2023-07-29",
  "hasService": "garbageCollection",
  "hasVehicle": "vehicle:WasteManagement:01",
  "hasDriver": "64d664d65ba90f270c40914a"
}
```

`hasRoute` : (reference) list of a waste bin container to reach.
`hasDay` : day on which the service occurred/will occuro
`hasService` : type of service
`hasVehicle` : (reference) vehicle involved in the service
`hasDriver` : (reference) employee assigned as driver

# Payment

```json
{
  "id": "4d6270c590f4d6640914a6ba",
  "payee": "64d664d65ba90f270c40914a",
  "amount": 100.00,
  "startPeriod" : "2023-01-01",
  "endPeriod" : "2024-12-31",
  "paid" : "2023-07-29 09:46:30",
  "status": "paid"
}
```

`payee` : (reference) id of holder of the payment, i.e. who is to pay
`startPeriod` : start of billing period
`endPeriod` : end of billing period
`paid` : timestamp of the payment, if made
`status` : status of the payment. It takes the following values:
- paid
- pending
- not paid
- expired
