import pika, sys, os 
import signal
import requests
import json
import logging


def setup_logging():
    # Create a logger object
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Set the log format
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # Create a log handler to print logs on stdout
    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)

    # Add the log manager to the logger
    logger.addHandler(stdout_handler)

    return logger

def handle_interrupt(signum, frame):
    """
    Handles interruption signal (CTRL+C).
    """
    print("Interrupted")
    sys.exit(0)



def main():

    logger = setup_logging()
    signal.signal(signal.SIGINT, handle_interrupt)# handle signal 


    # RabbitMQ connection

    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
    channel = connection.channel()

    #channel.queue_declare(queue='letterbox')
    channel.exchange_declare(exchange='gateway1', exchange_type='fanout')
    queue = channel.queue_declare(queue='', exclusive=True)
    

    def callback(ch, method, properties, body):
        #(" [x] Received %r" % json.loads(body))
        logger.info("Message received from gateway"+json.dumps(json.loads(body)))

        server_flask = 'iot-agent'
        endpoint = 'measure'
        port_server = '5000'

        url_req = 'http://'+server_flask+':'+port_server+'/'+endpoint
        myobj = json.loads(body)
        headers = {"Content-Type": "application/json"}
        response_mex = requests.post(url_req, json = myobj, headers=headers)
        print(response_mex.text)

    #channel.basic_consume(queue='letterbox', on_message_callback=callback, auto_ack=True)
    channel.queue_bind(exchange='gateway1', queue=queue.method.queue)
    channel.basic_consume(queue=queue.method.queue, auto_ack=True,on_message_callback=callback)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

