import pika
from pika.exchange_type import ExchangeType  #
from datetime import datetime
import time
import random
import json
import logging

random.seed()


def setup_logging():
    # Crea un oggetto logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Imposta il formato del log
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # Crea un gestore di log per stampare i log su stdout
    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)

    # Aggiungi il gestore di log al logger
    logger.addHandler(stdout_handler)

    return logger

logger = setup_logging()

# def generate_measure(id_dumpster,old_trash_level):
    
#   #dt = datetime.now()

#   #Trash level
#   trash_level=round(random.uniform(old_trash_level+ 0.05, old_trash_level+0.10),4)
#   if trash_level > 1:
#     trash_level = 1
#   if dt.hours == 4:
#     trash_level = 0.02

#   return [id_dumpster, dt, trash_level], dt


# only fot database population purpouse
def generate_measure(id_dumpster,old_trash_level, dt):
    
  #dt = datetime.now()
  new_ora = dt.hour+1
  new_giorno = dt.day
  if new_ora == 24:
    new_ora = 00
    new_giorno += 1

  dt = dt.replace(day=new_giorno, hour=new_ora)

  # Trash level -------------
  #Unsorted waste
  if id_dumpster == "000" or id_dumpster ==  "005":
    trash_level=round(random.uniform(old_trash_level+ 0.03, old_trash_level+0.08),4)

  if id_dumpster == "010" or id_dumpster ==  "015":
    trash_level=round(random.uniform(old_trash_level+ 0.07, old_trash_level+0.11),4)

  #Cardboard  waste
  if id_dumpster == "001" or id_dumpster ==  "006":
    trash_level=round(random.uniform(old_trash_level+ 0.05, old_trash_level+0.10),4)

  if id_dumpster == "011" or id_dumpster ==  "016":
    trash_level=round(random.uniform(old_trash_level+ 0.07, old_trash_level+0.14),4)
  
  #Plastic  waste
  if id_dumpster == "002" or id_dumpster ==  "007":
    trash_level=round(random.uniform(old_trash_level+ 0.05, old_trash_level+0.10),4)

  if id_dumpster == "012" or id_dumpster ==  "017":
    trash_level=round(random.uniform(old_trash_level+ 0.07, old_trash_level+0.14),4)
  
  #Glass and metals waste
  if id_dumpster == "003" or id_dumpster ==  "008":
    trash_level=round(random.uniform(old_trash_level+ 0.02, old_trash_level+0.07),4)

  if id_dumpster == "013" or id_dumpster ==  "018":
    trash_level=round(random.uniform(old_trash_level+ 0.04, old_trash_level+0.10),4)
  
  #Organic  waste
  if id_dumpster == "004" or id_dumpster ==  "009":
    trash_level=round(random.uniform(old_trash_level+ 0.07, old_trash_level+0.12),4)

  if id_dumpster == "014" or id_dumpster ==  "019":
    trash_level=round(random.uniform(old_trash_level+ 0.07, old_trash_level+0.10),4)


  if trash_level > 1:
    trash_level = 1
  if new_ora == 4:
    trash_level = 0.02

  return [id_dumpster, dt, trash_level], dt



def send_observation(observation):
  
  temp_idBin = "wastecontainer:Lecce:"+observation[0]
  message = {
    "idBin" : temp_idBin,
    "timestamp" : observation[1].strftime("%Y-%m-%d %H:%M:%S"),
    "fillingLevel" : observation[2]
  }

  

  logger.info("New measure generated: " + json.dumps(message))



  #channel.basic_publish(exchange='', routing_key='letterbox', body=json.dumps(message))
  channel.basic_publish(exchange='gateway1', routing_key='', body=json.dumps(message))



connection_parameters = pika.ConnectionParameters('rabbitmq')

connection = pika.BlockingConnection(connection_parameters)

channel = connection.channel()

#channel.queue_declare(queue='letterbox')
channel.exchange_declare(exchange='gateway1', exchange_type=ExchangeType.fanout)

#bin_list_id = ["000","001","002","003","004","005","006","007","008","009","010","011","012","013","014","015","016","017","018","019"]
bin_list_id = ["003","004","005","006","007","008","009","010","011","012","013","014","015","016","017","018","019"]
for id_bin in bin_list_id:

  if id_bin != "003":

    day_measure = datetime(2023, 7, 22, 8, 46, 30, 533252)

    observation = [id_bin, day_measure, 0.05] # starting observation

    for i in range(0,24):
      observation, day_measure = generate_measure(observation[0],observation[2],day_measure) #generate measure
      try:
        send_observation(observation)
      except Exception as e:
        print("Error occurred while sending observation:")
        print(e)
      
      time.sleep(7)

    time.sleep(3)
    day_measure = datetime(2023, 7, 25, 8, 46, 30, 533252)

    observation = [id_bin, day_measure, 0.05] # starting observation

    for i in range(0,24):
      observation, day_measure = generate_measure(observation[0],observation[2],day_measure) #generate measure
      try:
        send_observation(observation)
      except Exception as e:
        print("Error occurred while sending observation:")
        print(e)
      
      time.sleep(7)

  
  time.sleep(3)
  day_measure = datetime(2023, 7, 29, 8, 46, 30, 533252)

  observation = [id_bin, day_measure, 0.05] # starting observation

  for i in range(0,24):
    observation, day_measure = generate_measure(observation[0],observation[2],day_measure) #generate measure
    try:
      send_observation(observation)
    except Exception as e:
      print("Error occurred while sending observation:")
      print(e)
    
    time.sleep(7)
  

connection.close()