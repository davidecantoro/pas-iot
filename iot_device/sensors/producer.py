import pika, sys
from pika.exchange_type import ExchangeType  
from datetime import datetime, timedelta
import time
import random
import json
import logging
#import csv



def setup_logging():
    """
    Configures logging settings.
    """
    # Crea un oggetto logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Imposta il formato del log
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # Crea un gestore di log per stampare i log su stdout
    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)

    # Aggiungi il gestore di log al logger
    logger.addHandler(stdout_handler)

    return logger

def handle_interrupt(signum, frame):
    """
    Handles interruption signal (CTRL+C).
    """
    print("Interrupted")
    sys.exit(0)


class WasteContainer:
    def __init__(self, id, initial_trash_level, start_date, logger, channel):
        """
        Initializes a waste container.

        Args:
            id (str): Identifier of the waste container.
            initial_trash_level (float): Initial trash level of the container.
            start_date (datetime): Starting date for generating observations.
        """
        self.id = id
        self.trash_level = initial_trash_level
        self.current_date = start_date
        self.logger = logger
        self.channel = channel
        self.waste_disposal = 0
        self.citizen = None

# class WasteContainer
    def generate_measure(self):
        """
        Generates a new trash level observation based on the container's type and time.

        Returns:
            tuple: A tuple containing container ID, observation datetime, and trash level.
        """

        citizens = ["3n5T9kZxQ1", "B8vD2uL6Zg", "M7jK4pR0tS", "X1vY8zN5qW"]
        self.citizen = random.choice(citizens)

        # Determine the waste type based on the container ID
        # The difference in 'waste_types' with the suffix '_big' is only to differentiate the containers a bit
        if self.id in ["000", "005",]:
            waste_type = "unsorted"
        elif self.id in ["001", "006"]:
            waste_type = "cardboard"
        elif self.id in ["002", "007"]:
            waste_type = "plastic"
        elif self.id in ["003", "008"]:
            waste_type = "glass_metal"
        elif self.id in ["004", "009"]:
            waste_type = "organic"
        elif self.id in ["010", "015",]:
            waste_type = "unsorted_big"
        elif self.id in ["011", "016"]:
            waste_type = "cardboard_big"
        elif self.id in ["012", "017"]:
            waste_type = "plastic_big"
        elif self.id in ["013", "018"]:
            waste_type = "glass_metal_big"
        elif self.id in ["014", "019"]:
            waste_type = "organic_big"

        old_trash_level = self.trash_level

        
        # Calculate the trash level based on the waste type and time
        if waste_type == "unsorted":
            trash_level = round(random.uniform(self.trash_level + 0.03, self.trash_level + 0.08), 4)
        elif waste_type == "cardboard":
            trash_level = round(random.uniform(self.trash_level + 0.05, self.trash_level + 0.10), 4)
        elif waste_type == "plastic":
            trash_level = round(random.uniform(self.trash_level + 0.05, self.trash_level + 0.10), 4)
        elif waste_type == "glass_metal":
            trash_level = round(random.uniform(self.trash_level + 0.02, self.trash_level + 0.07), 4)
        elif waste_type == "organic":
            trash_level = round(random.uniform(self.trash_level + 0.07, self.trash_level + 0.12), 4)
        elif waste_type == "unsorted_big":
            trash_level = round(random.uniform(self.trash_level + 0.07, self.trash_level + 0.11), 4)
        elif waste_type == "cardboard_big":
            trash_level = round(random.uniform(self.trash_level + 0.07, self.trash_level + 0.14), 4)
        elif waste_type == "plastic_big":
            trash_level = round(random.uniform(self.trash_level + 0.07, self.trash_level + 0.14), 4)
        elif waste_type == "glass_metal_big":
            trash_level = round(random.uniform(self.trash_level + 0.04, self.trash_level + 0.10), 4)
        elif waste_type == "organic_big":
            trash_level = round(random.uniform(self.trash_level + 0.07, self.trash_level + 0.10), 4)

        self.trash_level = max(min(trash_level, 1.0), 0.0)
        self.waste_disposal = self.trash_level - old_trash_level

        #update time
        #self.current_date += timedelta(hours=1)

        return self.id, self.current_date, self.trash_level, self.waste_disposal,self.citizen
        

# class WasteContainer
    def send_observation(self):
        """
        Sends an observation message to the message broker.
        """
        temp_idBin = f"wastecontainer:Lecce:{self.id}"
        message = {
            "idBin": temp_idBin,
            "timestamp": self.current_date.strftime("%Y-%m-%d %H:%M:%S"),
            "fillingLevel": self.trash_level,
            "waste_disposal": self.waste_disposal,
            "citizen": self.citizen
        }

        self.logger.info("New measure generated: " + json.dumps(message))

        # Send the observation message to the RabbitMQ broker
        self.channel.basic_publish(exchange='gateway1', routing_key='', body=json.dumps(message))


class WasteManagementSimulator:
    def __init__(self, connection_params, container_ids):
        """
        Initializes the waste management simulator.

        Args:
            connection_params (pika.ConnectionParameters): Parameters for RabbitMQ connection.
            container_ids (list): List of waste container IDs.
        """
        self.connection_params = connection_params
        self.container_ids = container_ids
        self.connection = pika.BlockingConnection(self.connection_params)
        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange='gateway1', exchange_type=ExchangeType.fanout)
        self.logger = setup_logging()

    '''
    def simulate_old(self, days, hours):
        """
        Simulates trash level observations for specified days and hours.

        Args:
            days_to_simulate (int): Number of days to simulate.
            hours (int): Number of hours of observations per day.
        """
        for i in range(days):
            for j in range(hours):
                for container_id in self.container_ids:
                    container = WasteContainer(container_id, initial_trash_level=0.05, start_date=datetime.now(),logger=self.logger,channel=self.channel)
                    observation = container.generate_measure()
                    container.send_observation()
                    time.sleep(7)  # Delay between observations
                
                self.connection.process_data_events()


            time.sleep(3)  # Delay between days
            '''
    
    def simulate(self,days_to_simulate, start_date):
        """
        Continuously simulates trash level observations and sends them to the message broker.
        """

        start_date_iter = datetime.strptime(start_date, '%Y-%m-%dT%H:%M')
        end_date = start_date_iter + timedelta(days=days_to_simulate)
        #while True:
        while start_date_iter < end_date:
            for container_id in self.container_ids:
                container = WasteContainer(container_id, initial_trash_level=0.05, start_date=start_date_iter,logger=self.logger,channel=self.channel)
                observation = container.generate_measure()
                container.send_observation()
                start_date_iter += timedelta(minutes=30) 
                time.sleep(30) 
            
            self.connection.process_data_events()
            time.sleep(60)  

    def close_connection(self):
        """
        Closes the RabbitMQ connection.
        """
        self.connection.close()

'''    # Estimated time required to generate (20*370) 7400 measurements = 7770 seconds (approx. 2 hours 9.5 minutes)
    # Estimated time = days * (number of bin + 1)

    def simulate_to_csv_by_date(self):
        """
        Simulates trash level observations and saves them to a CSV file.
        """
        
        ts_list = []
        current_date = datetime(2023, 1, 1)  # Start date 01 January 2023
        for i in range(370): # 370 days
            for container_id in self.container_ids:
                container = WasteContainer(container_id, initial_trash_level=0.05, start_date=datetime.now(),logger=self.logger,channel=self.channel)
                observation = container.generate_measure()
        
                # Cleaning waste container every 5 days
                if i % 5 == 0:
                    container.trash_level = 0

                temp_idBin = f"wastecontainer:Lecce:{observation[0]}"
                message = {
                    "idBin": temp_idBin,
                    "timestamp": observation[1].strftime("%Y-%m-%d %H:%M:%S"),
                    "fillingLevel": observation[2],
                    "waste_disposal": observation[3],
                    "citizen": observation[4]
                }

                self.logger.info("New measure generated: " + json.dumps(message))
                
                ts_list.append(message)
                time.sleep(1) 

            self.connection.process_data_events()
            time.sleep(1)

            current_date += timedelta(days=1)  # Increment day
        
        # Save ts_list to a CSV file
        with open('ts_wastecontainer.csv', 'w') as f:
            f.write(json.dumps(ts_list))'''




def main():
    container_ids = ["000","001","002","003", "004", "005", "006", "007", "008", "009", "010", "011", "012", "013", "014", "015", "016", "017", "018", "019"]
    connection_parameters = pika.ConnectionParameters('rabbitmq')
    
    setup_logging()

    simulator = WasteManagementSimulator(connection_parameters, container_ids)
    
    
    try:
        #simulator.simulate(days_to_simulate=3, hours_per_day=24)
        simulator.simulate(days_to_simulate=1, start_date='2024-01-01T06:00')
        #simulator.simulate_to_csv_by_date()
    except Exception as e:
        print("An error occurred:", e)
    finally:
        simulator.close_connection()


if __name__ == "__main__":
    main()