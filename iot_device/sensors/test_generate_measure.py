import unittest
from producer import WasteContainer
from datetime import datetime

# unit test
class TestWasteContainer(unittest.TestCase):

    def setUp(self):
        self.logger = DummyLogger()
        self.channel = DummyChannel()
        self.current_date = datetime.now()
        self.container = WasteContainer("001", 0.05, self.current_date, self.logger, self.channel)

    def test_generate_measure(self):
        result = self.container.generate_measure()
        self.assertIsInstance(result[0], str)
        self.assertIsInstance(result[1], datetime)
        self.assertIsInstance(result[2], float)

class DummyLogger:
    def __init__(self):
        self.info_called = False

    def info(self, message):
        self.info_called = True

class DummyChannel:
    pass

if __name__ == '__main__':
    unittest.main()
